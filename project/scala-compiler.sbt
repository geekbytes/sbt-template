scalacOptions := Seq(
  //"-Xlint:-unused",
  "-unchecked",
  "-deprecation",
  "-feature",
  "-language:higherKinds",
  "-Ypartial-unification",
  "-Yrangepos",
  "-Ywarn-unused-import",
  //"-P:semanticdb:exclude:(Macros|program)"
  /*,
  "-Ywarn-unused", "-Ywarn-unused-import" ,
  "-Ymacro-debug-lite"*/
)
