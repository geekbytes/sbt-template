/*
 * Copyright (c) 2020.
 * Manu T S [http://geekbytes.io]
 */

import sbt.Keys._
import sbt._

object Common {

  val settings = Seq(
    scalaVersion := Globals.SCALA_VERSION,
    libraryDependencies ++= Dependencies.common.*,
    resolvers ++= Globals.RESOLVERS
  )

  val appSettings = settings ++ Seq(
    libraryDependencies ++= Dependencies.app.*
  )

}
